Description: <short summary of the patch>
 TODO: Put a short summary on the line above and replace this paragraph
 with a longer explanation of this change. Complete the meta-information
 with other relevant fields (see below for details). To make it easier, the
 information below has been extracted from the changelog. Adjust it or drop
 it.
 .
 node-d3-time (1.1.0-1) UNRELEASED; urgency=low
 .
   * Initial release (Closes: #nnnn)
   *
Author: achaayan <alphishajan007@gmail.com>

---
The information above should follow the Patch Tagging Guidelines, please
checkout http://dep.debian.net/deps/dep3/ to learn about the format. Here
are templates for supplementary fields that you might want to add:

Origin: <vendor|upstream|other>, <url of original patch>
Bug: <url in upstream bugtracker>
Bug-Debian: https://bugs.debian.org/<bugnumber>
Bug-Ubuntu: https://launchpad.net/bugs/<bugnumber>
Forwarded: <no|not-needed|url proving that it has been forwarded>
Reviewed-By: <name and email of someone who approved the patch>
Last-Update: 2020-03-27

--- node-d3-time-1.1.0.orig/rollup.config.js
+++ node-d3-time-1.1.0/rollup.config.js
@@ -1,4 +1,4 @@
-import {terser} from "rollup-plugin-terser";
+
 import * as meta from "./package.json";
 
 const config = {
@@ -18,19 +18,4 @@ const config = {
 
 export default [
   config,
-  {
-    ...config,
-    output: {
-      ...config.output,
-      file: `dist/${meta.name}.min.js`
-    },
-    plugins: [
-      ...config.plugins,
-      terser({
-        output: {
-          preamble: config.output.banner
-        }
-      })
-    ]
-  }
 ];
